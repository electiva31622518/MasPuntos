# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class BolsaPunto(models.Model):
    cod_bolsa = models.IntegerField(primary_key=True)
    cod_cliente = models.ForeignKey('Cliente', models.DO_NOTHING, db_column='cod_cliente')
    fecha_asig_punto = models.DateField()
    fecha_fin_punto = models.DateField()
    punto_asignado = models.IntegerField()
    punto_utilizado = models.IntegerField()
    saldo_punto = models.IntegerField()
    monto = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'bolsa_punto'


class Cliente(models.Model):
    cod_cliente = models.IntegerField(primary_key=True)
    nombre = models.CharField()
    apellido = models.CharField()
    documento = models.IntegerField()
    tipo_documento = models.IntegerField()
    nacionalidad = models.CharField()
    telefono = models.IntegerField(blank=True, null=True)
    email = models.CharField(blank=True, null=True)
    fecha_nacimiento = models.DateField(blank=True, null=True)

    def __str__(self) -> str:
         return str(self.nombre)
    
    def __str__(self) -> str:
         return str(self.apellido)
    
    def __str__(self) -> str:
         return str(self.documento)

    class Meta:
        #managed = False
        db_table = 'cliente'


class ConceptoUsoPunto(models.Model):
    cod_concepto_uso_punto = models.IntegerField(primary_key=True)
    concepto = models.CharField()
    punto_requerido = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'concepto_uso_punto'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class ParamVencPunto(models.Model):
    cod_param_venc_punto = models.IntegerField(primary_key=True)
    fecha_ini_validez = models.DateField()
    fecha_fin_validez = models.DateField()
    dias_duracion = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'param_venc_punto'


class ReglaAsigPunto(models.Model):
    cod_regla = models.IntegerField(primary_key=True)
    limite_inferior = models.IntegerField()
    limite_superior = models.IntegerField()
    monto_un_punto = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'regla_asig_punto'


class UsoPuntoCab(models.Model):
    cod_uso_punto_cab = models.IntegerField(primary_key=True)
    cod_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='cod_cliente')
    fecha = models.DateField()
    fecha_fin_punto = models.DateField()
    cod_concepto_uso_punto = models.ForeignKey(ConceptoUsoPunto, models.DO_NOTHING, db_column='cod_concepto_uso_punto')

    class Meta:
        managed = False
        db_table = 'uso_punto_cab'


class UsoPuntoDet(models.Model):
    cod_uso_punto_det = models.IntegerField(primary_key=True)
    cod_uso_punto_cab = models.ForeignKey(UsoPuntoCab, models.DO_NOTHING, db_column='cod_uso_punto_cab')
    punto_utilizado = models.IntegerField()
    cod_bolsa = models.ForeignKey(BolsaPunto, models.DO_NOTHING, db_column='cod_bolsa')

    class Meta:
        managed = False
        db_table = 'uso_punto_det'
