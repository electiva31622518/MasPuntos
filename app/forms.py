from django import forms
from .models import Cliente, BolsaPunto, UsoPuntoCab

#Cliente
class ClienteModelForm(forms.ModelForm):
     class Meta:
        model = Cliente
        fields = ("nombre","apellido","fecha_nacimiento","documento")



class BolsaPuntosModelForm(forms.ModelForm):
   class Meta:
        model = BolsaPunto
        fields = ("cod_cliente","fecha_asig_punto","fecha_fin_punto","punto_asignado")
