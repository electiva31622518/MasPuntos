from django.contrib import admin
from . import models

# Register your models here.

admin.site.register(models.Cliente)
admin.site.register(models.BolsaPunto)
admin.site.register(models.ConceptoUsoPunto)
admin.site.register(models.ParamVencPunto)
admin.site.register(models.UsoPuntoCab)
admin.site.register(models.UsoPuntoDet)
admin.site.register(models.ReglaAsigPunto)
